# TESTWORK 738333

## instalation
* Install all dependencies:
```
    npm i
    composer i
```
* Create new database
* Create new .env file by .env.example
* Make DB migration and seed:
```
php artisan migrate --seed
```
* Run app:
```
php artisan serve
```
* Import postmanCollection.json in your Postman and send requests