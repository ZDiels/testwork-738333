<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Product;
use App\Models\Order;
use Illuminate\Support\Facades\DB;

// use App\Models\;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();

        Product::factory(50)->create();
        Order::factory(10)->create();

        for ($i = 1; $i <= 10; $i++) {
            for ($prod = 1; $prod <= rand(1, 10); $prod++) {
                DB::table('orders_products')->insert([
                    'order_id' => $i,
                    'product_id' => rand(1, 50),
                ]);
            }
        }
    }
}
