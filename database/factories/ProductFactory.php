<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Product>
 */
class ProductFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'name' => $this->faker->sentence(6, true),
            'slug' => $this->faker->unique()->word(),
            'price' => rand(1, 7000),
            'description' => $this->faker->sentence(),
            'isRecomended' => $this->faker->boolean(),
        ];
    }
}
