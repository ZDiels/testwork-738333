<?php
namespace App\Repositories;

use App\Http\Requests\ProductRequest;
use App\Models\Product;
use App\Models\Order;
use App\Repositories\Interfaces\ProductRepositoryInterface;

class ProductRepository implements ProductRepositoryInterface
{
    public function all(ProductRequest $request)
    {
			if ($request->has('order')) {
					return $this->getByOrder($request->input('order'));
			}

			$filter = array_filter($request->all(['name', 'price', 'slug', 'isRecomended']), 'strlen');

			$products = Product::query();
			$products->where($filter);

			$orderBy = $request->has('sort_by') ? $request->input('sort_by') : 'id';
			$order = $request->has('sort') ? $request->input('sort') : 'asc';

			$products->orderBy($orderBy, $order);
			return $products->get();
    }

		public function createProduct ($data) {
			return Product::create($data);
		}

		public function getProduct($productId)
		{
			return Product::findOrFail($productId);
		}

		public function updateProduct(ProductRequest $request, $productId)
		{
			$element = Product::findOrFail($productId);
			$element->fill($request->except(['id']));
			$element->save();
			return response()->json($element);
		}

		public function removeProduct($productId)
		{
			$element = Product::findOrFail($productId);
			if($element->delete()) return response(null, 204);
		}

    public function getByOrder($orderId)
    {
			// return [$orderId];
        return Order::findOrFail($orderId)->products;
    }
}