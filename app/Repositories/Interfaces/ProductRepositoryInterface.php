<?php
namespace App\Repositories\Interfaces;

use App\Http\Requests\ProductRequest;

interface ProductRepositoryInterface
{
    public function all (ProductRequest $request);
    public function getByOrder ($orderId);
    public function createProduct ($data);
    public function getProduct ($productId);
    public function updateProduct (ProductRequest $request, $productId);
    public function removeProduct ($productId);
}