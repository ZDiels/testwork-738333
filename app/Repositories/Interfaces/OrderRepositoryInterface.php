<?php
namespace App\Repositories\Interfaces;

use App\Http\Requests\OrderRequest;

interface OrderRepositoryInterface
{
    public function all (OrderRequest $request);
    public function createOrder (OrderRequest $request);
    public function getOrder ($orderId);
    public function updateOrder (OrderRequest $request, $orderId);
    public function removeorder ($orderId);
}