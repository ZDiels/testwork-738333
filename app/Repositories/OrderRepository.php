<?php
namespace App\Repositories;

use App\Http\Requests\OrderRequest;
use App\Models\Order;
use App\Models\Product;
use App\Repositories\Interfaces\OrderRepositoryInterface;

class OrderRepository implements OrderRepositoryInterface
{
    public function all(OrderRequest $request)
    {
			$filter = array_filter($request->all(['client', 'isFinished']), 'strlen');

			$orders = Order::query();
			$orders->where($filter);

			$orderBy = $request->has('sort_by') ? $request->input('sort_by') : 'id';
			$order = $request->has('sort') ? $request->input('sort') : 'asc';

			$orders->orderBy($orderBy, $order);
			return $orders->get();
    }

		public function createOrder (OrderRequest $request) {
			$data = $request->validated();
			$order = Order::create($data);
			if ($request->has('products')) {
				foreach(explode(',', $request->input('products')) as $productID) {
					$order->products()->attach($productID);
				}
			}
			return $order;

			// return ['ok'];
		}

		public function getOrder($productId)
		{
			return Order::findOrFail($productId);
		}

		public function updateOrder(OrderRequest $request, $productId)
		{
			$element = Order::findOrFail($productId);
			$element->fill($request->except(['id']));
			$element->save();
			return response()->json($element);
		}

		public function removeOrder($productId)
		{
			$element = Order::findOrFail($productId);
			if($element->delete()) return response(null, 204);
		}
}