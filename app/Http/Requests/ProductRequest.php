<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class ProductRequest extends FormRequest
{
  /**
   * Determine if the user is authorized to make this request.
   *
   * @return bool
   */
  public function authorize()
  {
    return true; // change it when add auth
  }

  /**
   * Get the validation rules that apply to the request.
   *
   * @return array
   */
  public function rules()
  {
    $rules = [
      'slug' => 'required|string|unique:products,slug',
      'name' => 'required|string',
      'description' => '',
      'price' => 'required|min:1|max:99999999',
      'isRecomended' => 'boolean'
    ];

    switch ($this->getMethod()) {
      case 'POST':
        return $rules;
      case 'PUT':
        return [
          // 'id' => 'required|integer|exists:products,id',
          'slug' => [
            'required',
            Rule::unique('products')->ignore($this->slug, 'slug')
          ]
        ] + $rules;
        // case 'PATCH':
      case 'DELETE':
        return [
          'id' => 'required|integer|exists:products,id'
        ];
      case 'GET':
        return [];
    }
  }

  /**
   * Add params from URL
   *
   * @param array $keys
   * @return array
   */
  public function all($keys = null)
  {
    // return $this->all();
    $data = parent::all($keys);
    switch ($this->getMethod()) {
        // case 'PUT':
        // case 'PATCH':
      case 'DELETE':
        $data['date'] = $this->route('day');
    }
    return $data;
  }
}
