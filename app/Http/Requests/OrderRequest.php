<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;


class OrderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true; // change it when add auth
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
      $rules = [
        'client' => 'required|string',
        'isFinished' => 'boolean',
      ];

      switch ($this->getMethod())
      {
        case 'POST':
        case 'PUT':
          return $rules;
        case 'DELETE':
          return [
              'id' => 'required|integer|exists:orders,id'
          ];
        case 'GET':
          return [];
      }
    }

    /**
     * Add params from URL
     *
     * @param array $keys
     * @return array
     */
    public function all($keys = null)
    {
      // return $this->all();
      $data = parent::all($keys);
      switch ($this->getMethod())
      {
        // case 'PUT':
        // case 'PATCH':
        case 'DELETE':
          $data['date'] = $this->route('day');
      }
      return $data;
    }
}
