<?php

namespace App\Http\Controllers;

use App\Repositories\Interfaces\OrderRepositoryInterface;
use App\Http\Requests\OrderRequest;
use App\Models\Order;

class OrderController extends Controller
{
    private $orderRepository;

    public function __construct(OrderRepositoryInterface $orderRepository)
    {
        $this->orderRepository = $orderRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(OrderRequest $request)
    {
        return $this->orderRepository->all($request);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\OrderRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(OrderRequest $request)
    {
        return $this->orderRepository->createOrder($request);
    }

    /**
     * Display the specified resource.
     *
     * @param  integer $Order
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return $this->orderRepository->getOrder($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\OrderRequest  $request
     * @param  \App\Models\Order  $Order
     * @return \Illuminate\Http\Response
     */
    public function update(OrderRequest $request, $id)
    {
        return $this->orderRepository->updateOrder($request, $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Order  $Order
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->orderRepository->removeOrder($id);
    }
}
